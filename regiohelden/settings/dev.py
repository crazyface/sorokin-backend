from .base import *

DEBUG = True

INSTALLED_APPS += (
    'debug_toolbar',
)

INTERNAL_IPS = ('127.0.0.1', )

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


GOOGLE_OAUTH2_SOCIAL_AUTH_RAISE_EXCEPTIONS = DEBUG
SOCIAL_AUTH_RAISE_EXCEPTIONS = DEBUG
RAISE_EXCEPTIONS = DEBUG