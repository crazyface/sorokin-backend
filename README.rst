Getting Started
------------------------

First clone the repository::

    $ git clone git@bitbucket.org:crazyface/regiohelden.git
    $ cd regiohelden

To setup your local environment you should create a virtualenv and install the
necessary requirements::

    $ mkvirtualenv sorokin -p `which python3.4`
    (sorokin)$ pip install -r requirements/dev.txt

Next, we'll set up our local environment variables.::

    (sorokin)$ echo "export DJANGO_SETTINGS_MODULE=regiohelden.settings.local" >> $VIRTUAL_ENV/bin/postactivate
    (sorokin)$ echo "unset DJANGO_SETTINGS_MODULE" >> $VIRTUAL_ENV/bin/postdeactivate
    (sorokin)$ deactivate
    (sorokin)$ workon sorokin

Run the initial migrate::

    (sorokin)$ python manage.py migrate
    (sorokin)$ python manage.py runserver

