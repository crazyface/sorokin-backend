from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from api.accounts.views import UserViewSet
from api.clients.views import ClientViewSet

router = DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'user', UserViewSet)


urlpatterns = [
]

urlpatterns += router.urls
