from rest_framework import serializers
from clients.models import Client
from django.core.files.storage import default_storage


class ClientSerializer(serializers.ModelSerializer):
    avatar_url = serializers.SerializerMethodField()
    avatar = serializers.CharField(required=False, allow_blank=True,
                                   allow_null=True)

    def validate_avatar(self, attr):
        if attr:
            return default_storage.open(attr)

    def get_avatar_url(self, obj):
        if obj.avatar:
            url = obj.avatar.url
            request = self.context.get('request')
            if request:
                return request.build_absolute_uri(url)
            return url

    class Meta:
        read_only_fields = ['user']
        model = Client
