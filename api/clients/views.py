from rest_framework.decorators import api_view, list_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from api.clients.serializers import ClientSerializer
from clients.models import Client
from rest_framework import permissions, status
from django.core.files.storage import default_storage
from django.conf import settings
import os
from PIL import Image


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        qs = super(ClientViewSet, self).get_queryset()
        return qs.owned_by(self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

    @list_route(['POST'])
    def upload(self, request):
        file = request.FILES['file']
        try:
            Image.open(file)
        except:
            return Response(
                {'error': '"{}" is not valid image file.'.format(file.name)},
                status=status.HTTP_400_BAD_REQUEST)

        res = default_storage.save(os.path.join('tmp', file.name), file)
        url = request.build_absolute_uri('{}{}'.format(settings.MEDIA_URL,
                            res))
        return Response({'url': url,
                         'path': res})
