from django.db import models
from django.conf import settings
from localflavor.generic.models import IBANField


class ClientQueryset(models.QuerySet):
    def owned_by(self, user):
        return self.filter(user=user)


class Client(models.Model):
    objects = ClientQueryset.as_manager()

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    avatar = models.ImageField(upload_to='avatars', null=True, blank=True)
    iban = IBANField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


